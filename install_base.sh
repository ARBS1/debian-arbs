#!/usr/bin/env bash
#
# This script will install the base programs for Ubuntu

# General prerequisites
echo ''
echo '########## Install general prerequisites #############'
apt update -qq && apt upgrade -y -qq
apt install -y -qq \
        ncurses-term \
        nodejs \
        npm \
        powerline \
        ranger \
        software-properties-common \
        stow \
        wget

# Neovim prerequisites
echo ''
echo '########## Install Neovim prerequisites ##########'
add-apt-repository ppa:neovim-ppa/unstable -y
apt update -qq
apt install -y -qq \
        neovim \
        python3 \
        python3-pynvim \
        yarn \
        xclip

curl -sL install-node.vercel.app/lts | bash

npm install remark-cli
npm install remark-gfm

# ZSH prerequisites
echo ''
echo '########## Install ZSH prerequisites ##########'
apt install -y -qq \
        fzf \
        zsh

wget https://raw.githubusercontent.com/junegunn/fzf/master/shell/completion.zsh -P /usr/share/fzf/
wget https://raw.githubusercontent.com/junegunn/fzf/master/shell/key-bindings.zsh -P /usr/share/fzf

echo ''
echo '########## Change shell ##########'
chsh -s /bin/zsh root
chsh -s /bin/zsh $USER

# Tmux prerequisites
echo ''
echo '########## Install tmux ##########'
apt install -y -qq \
        tmux

# Other tools
echo ''
echo '########## Install other tools ##########'
apt install -y -qq \
        bat \
        dnsutils

wget https://github.com/lsd-rs/lsd/releases/download/v1.1.5/lsd-musl_1.1.5_$(dpkg --print-architecture).deb
dpkg -i lsd-musl_1.1.5_$(dpkg --print-architecture).deb