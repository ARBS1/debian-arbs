#!/usr/bin/env bash
#
# This script will install the base program for Ubuntu

# Neovim prerequisites
echo ''
echo '########## Install Neovim prerequisites ##########'
apt install -y -qq \
        autoconf \
        automake \
        cmake \
        g++ \
        gettext \
        libtool \
        libtool-bin \
        ninja-build \
        pkg-config \
        python3 \
        python3-pynvim \
        unzip \
        xclip \
        yarn 

curl -sL install-node.vercel.app/lts | bash

npm install remark-cli
npm install remark-gfm

git clone https://github.com/neovim/neovim

cd neovim 
git checkout release-0.8
make CMAKE_BUILD_TYPE=RelWithDebInfo
make install


